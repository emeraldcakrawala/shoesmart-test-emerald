final String tableUsers = 'user';

class NoteFields {
  static final List<String> values = [
    id,
    email,
    password,
  ];
  static final String id = '_id';
  static final String email = 'email';
  static final String password = 'password';
}

class User {
  final int? id;
  final String email;
  final String password;

  const User({
    this.id,
    required this.email,
    required this.password,
  });

  User copy({
    int? id,
    String? email,
    String? password,
  }) =>
      User(
        id: id ?? this.id,
        email: email ?? this.email,
        password: password ?? this.password,
      );

  static User fromJson(Map<String, Object?> json) => User(
        id: json[NoteFields.id] as int?,
        email: json[NoteFields.email] as String,
        password: json[NoteFields.password] as String,
      );

  Map<String, Object?> toJson() => {
        NoteFields.id: id,
        NoteFields.email: email,
        NoteFields.password: password,
      };
}
