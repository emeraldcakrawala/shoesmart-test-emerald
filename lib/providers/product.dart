import 'package:flutter/material.dart';

class Product with ChangeNotifier {
  final String id;
  final String productName;
  final String brandName;
  final String description;
  final double price;
  final String imageAsset;
  bool isFavorite;

  Product({
    required this.id,
    required this.productName,
    required this.brandName,
    required this.description,
    required this.price,
    required this.imageAsset,
    this.isFavorite = false,
  });

  void toggleFavoriteStatus() {
    isFavorite = !isFavorite;
    notifyListeners();
  }
}
