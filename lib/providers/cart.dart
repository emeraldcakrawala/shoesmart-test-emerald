import 'package:flutter/material.dart';

class CartItem {
  final String id;
  final String productName;
  final String brandName;
  final int quantity;
  final double price;
  final String imageAsset;

  CartItem({
    required this.id,
    required this.productName,
    required this.brandName,
    required this.quantity,
    required this.price,
    required this.imageAsset,
  });
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get itemCount {
    return _items.length;
  }

  double get totalAmount {
    return _items.values.fold(0.0, (previousValue, cartItem) {
      return previousValue + (cartItem.price * cartItem.quantity);
    });
  }

  void removeItem(String productId) {
    _items.remove(productId);

    notifyListeners();
  }

  void removeSingleItem(String productId) {
    if (!_items.containsKey(productId)) {
      return;
    }

    if (_items[productId]!.quantity > 1) {
      _items.update(
        productId,
        (existingCartItem) => CartItem(
            id: existingCartItem.id,
            price: existingCartItem.price,
            productName: existingCartItem.productName,
            brandName: existingCartItem.brandName,
            quantity: existingCartItem.quantity - 1,
            imageAsset: existingCartItem.imageAsset),
      );

      notifyListeners();
    } else {
      _items.remove(productId);
    }

    notifyListeners();
  }

  void addItem({
    required String productId,
    required double price,
    required String productName,
    required String brandName,
    required String imageAsset,
  }) {
    if (_items.containsKey(productId)) {
      _items[productId] = _items.update(
        productId,
        (existingCartItem) => CartItem(
            id: existingCartItem.id,
            price: existingCartItem.price,
            productName: existingCartItem.productName,
            brandName: existingCartItem.brandName,
            quantity: existingCartItem.quantity + 1,
            imageAsset: existingCartItem.imageAsset),
      );
    } else {
      _items.putIfAbsent(
        productId,
        () => CartItem(
            id: DateTime.now().toString(),
            productName: productName,
            brandName: brandName,
            price: price,
            quantity: 1,
            imageAsset: imageAsset),
      );
    }

    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}
