import 'package:flutter/material.dart';
import './product.dart';

class Products with ChangeNotifier {
  // ignore: prefer_final_fields
  List<Product> _items = [
    Product(
      id: 'sb1',
      productName: 'Court Vision Shoes',
      brandName: 'Nike',
      description: 'Free shipping',
      price: 200000,
      imageAsset: 'assets/shoes/nike/1.png',
    ),
    Product(
      id: 'sb2',
      productName: 'ULTRABOOST 21',
      brandName: 'Adidas',
      description: 'Free shipping',
      price: 500000,
      imageAsset: 'assets/shoes/adidas/1.png',
    ),
    Product(
      id: 'sb3',
      productName: 'Puma Shuffle Womens Sneakers',
      brandName: 'Puma',
      description: 'Free shipping',
      price: 499000,
      imageAsset: 'assets/shoes/puma/1.png',
    ),
    Product(
      id: 'sb4',
      productName: 'Karimata Trail RayaJing9a',
      brandName: 'Yongki Komaladi',
      description: 'Free shipping',
      price: 259999,
      imageAsset: 'assets/shoes/yk/1.png',
    ),
    Product(
      id: 'sb5',
      productName: 'Marks & Spencer',
      brandName: 'New Era',
      description: 'Free shipping',
      price: 293999,
      imageAsset: 'assets/shoes/newera/1.png',
    ),
    Product(
      id: 'sb11',
      productName: 'Victory One Slides',
      brandName: 'Nike',
      description: 'Free shipping',
      price: 29999,
      imageAsset: 'assets/shoes/nike/2.png',
    ),
    Product(
      id: 'sb12',
      productName: 'ULTRABOOST 22',
      brandName: 'Adidas',
      description: 'Free shipping',
      price: 129999,
      imageAsset: 'assets/shoes/adidas/2.png',
    ),
    Product(
      id: 'sb13',
      productName: 'Puma Incinerate Womens Running Shoes - Black',
      brandName: 'Puma',
      description: 'Free shipping',
      price: 529999,
      imageAsset: 'assets/shoes/puma/2.png',
    ),
    Product(
      id: 'sb14',
      productName: 'Karimata Navy Trail BT',
      brandName: 'Yongki Komaladi',
      description: 'Free shipping',
      price: 629999,
      imageAsset: 'assets/shoes/yk/2.png',
    ),
    Product(
      id: 'sb15',
      productName: 'Hoka',
      brandName: 'New Era',
      description: 'Free shipping',
      price: 289099,
      imageAsset: 'assets/shoes/newera/2.png',
    ),
    Product(
      id: 'sb21',
      productName: 'Court Vision Mid',
      brandName: 'Nike',
      description: 'Free shipping',
      price: 929999,
      imageAsset: 'assets/shoes/nike/3.png',
    ),
    Product(
      id: 'sb22',
      productName: 'Ultraboost COLD RDY LAB',
      brandName: 'Adidas',
      description: 'Free shipping',
      price: 52000,
      imageAsset: 'assets/shoes/adidas/3.png',
    ),
    Product(
      id: 'sb23',
      productName: 'Puma Incinerate Womens Running Shoes - Pink',
      brandName: 'Puma',
      description: 'Free shipping',
      price: 53000,
      imageAsset: 'assets/shoes/puma/3.png',
    ),
    Product(
      id: 'sb24',
      productName: 'Karimata Yellow Trail BT',
      brandName: 'Yongki Komaladi',
      description: 'Free shipping',
      price: 54000,
      imageAsset: 'assets/shoes/yk/3.png',
    ),
    Product(
      id: 'sb25',
      productName: 'DR. MARTENS',
      brandName: 'New Era',
      description: 'Free shipping',
      price: 55000,
      imageAsset: 'assets/shoes/newera/3.png',
    ),
    Product(
      id: 'sb211',
      productName: 'Victory One',
      brandName: 'Nike',
      description: 'Free shipping',
      price: 56000,
      imageAsset: 'assets/shoes/nike/4.png',
    ),
    Product(
      id: 'sb212',
      productName: 'ULTRABOOST DNA GUARD',
      brandName: 'Adidas',
      description: 'Free shipping',
      price: 57000,
      imageAsset: 'assets/shoes/adidas/4.png',
    ),
    Product(
      id: 'sb213',
      productName: 'Puma Aviator Mens Running Shoes',
      brandName: 'Puma',
      description: 'Free shipping',
      price: 58000,
      imageAsset: 'assets/shoes/puma/4.png',
    ),
    Product(
      id: 'sb214',
      productName: 'Karimata Maroon Trail BT',
      brandName: 'Yongki Komaladi',
      description: 'Free shipping',
      price: 59000,
      imageAsset: 'assets/shoes/yk/4.png',
    ),
    Product(
      id: 'sb215',
      productName: 'Beajove',
      brandName: 'New Era',
      description: 'Free shipping',
      price: 51000,
      imageAsset: 'assets/shoes/newera/4.png',
    ),
  ];

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favItems {
    return _items.where((prodItem) => prodItem.isFavorite).toList();
  }

  List<Product> get sortCheapest {
    _items.sort((a, b) => a.price.compareTo(b.price));
    return _items;
  }

  List<Product> getRange({required double rangeUp, required double rangeDown}) {
    return _items
        .where((prodItem) =>
            prodItem.price >= rangeDown && prodItem.price <= rangeUp)
        .toList();
  }

  Product findById(String id) {
    return _items.firstWhere((element) => element.id == id);
  }

  void removeSingleItem(String id) {
    final isProductFound = _items.singleWhere((prod) => prod.id == id);

    if (isProductFound != null) {
      _items.removeWhere((prod) => prod.id == id);
    }

    notifyListeners();
  }

  void addProduct(Product addedProduct) {
    _items.add(addedProduct);

    notifyListeners();
  }

  void editProduct(String id, Product editedProduct) {
    final index = _items.indexWhere((prod) => prod.id == id);

    if (index >= 0) {
      _items[index] = editedProduct;
    }

    notifyListeners();
  }
}
