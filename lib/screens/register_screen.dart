import 'package:flutter/material.dart';
import 'package:shoesmart_test/database/database.dart';
import 'package:shoesmart_test/providers/user.dart';

import '../shared/shared.dart';
import 'login_screen.dart';

class RegisterScreen extends StatefulWidget {
  static const routerName = '/register';
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool hidePass = true;

  Future<void> addNote(
      {required String email, required String password}) async {
    final user = User(
      email: email,
      password: password,
    );

    await UserDatabase.instance.create(user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white38,
        
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Form(
            key: _form,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.book,
                  size: 70,
                  color: Colors.black54,
                ),
                const Text(
                  "REGISTER",
                  style: TextStyle(color: Colors.black54, fontSize: 30),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 30),
                          child: Text(
                            'Email',
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.black12),
                          width: double.infinity,
                          margin: const EdgeInsets.fromLTRB(26, 5, 26, 0),
                          child: Container(
                            width: double.infinity,
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: TextFormField(
                              controller: emailController,
                              validator: (val) {
                                bool emailValid = RegExp(
                                        r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$')
                                    .hasMatch(emailController.text);

                                if (val!.isEmpty) {
                                  return 'Input your email';
                                }
                                if (emailValid == false) {
                                  return 'format is wrong';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Your email'),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 30),
                          child: Text(
                            'Password',
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.black12),
                          width: double.infinity,
                          padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                          margin: const EdgeInsets.fromLTRB(25, 5, 26, 0),
                          child: SizedBox(
                            width: double.infinity,
                            child: TextFormField(
                              controller: passwordController,
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return 'input your password';
                                }
                                return null;
                              },
                              obscureText: hidePass,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Password',
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    hidePass
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                    color: Colors.black,
                                    size: 20,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      hidePass = !hidePass;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(18, 25, 18, 10),
                          child: Container(
                              width: double.infinity,
                              padding: const EdgeInsets.all(8),
                              child: ElevatedButton(
                                  child: const Padding(
                                    padding: EdgeInsets.all(14.0),
                                    child: Text(
                                      'Register',
                                    ),
                                  ),
                                  onPressed: () {
                                    if (_form.currentState!.validate() ==
                                        true) {
                                      addNote(
                                          email: emailController.text,
                                          password: passwordController.text);

                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  const LoginScreen()),
                                          (Route<dynamic> route) =>
                                              route is LoginScreen);
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                          content:
                                              Text('successful registration'),
                                        ),
                                      );
                                    }
                                  },
                                  style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              whiteColor),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black87),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      ))))),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
