import 'package:flutter/material.dart';
import 'package:shoesmart_test/screens/account_screen.dart';
import 'package:shoesmart_test/screens/favorite_screen.dart';
import '../shared/shared.dart';
import '../widgets/button_navbar.dart';
import 'products_overview_screen.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int selectedPage = 0;
  PageController pageController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backroundColor,
      body: Stack(
        children: [
          SafeArea(child: Container(color: backroundColor)),
          SafeArea(
              child: PageView(
            physics: const NeverScrollableScrollPhysics(),
            controller: pageController,
            onPageChanged: (index) {
              setState(() {
                selectedPage = index;
              });
            },
            children: const [
              ProductsOverviewScreen(),
              FavoriteScreen(),
              AccountScreen()
            ],
          )),
          Align(
            alignment: Alignment.bottomCenter,
            child: ButtonNavBar(
              onTap: (index) {
                setState(() {
                  selectedPage = index;
                });
                pageController.jumpToPage(selectedPage);
              },
              selectedIndex: selectedPage,
            ),
          )
        ],
      ),
    );
  }
}
