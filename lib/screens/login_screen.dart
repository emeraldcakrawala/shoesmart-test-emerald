import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shoesmart_test/database/database.dart';
import 'package:shoesmart_test/providers/user.dart';
import 'package:shoesmart_test/screens/register_screen.dart';

import '../shared/shared.dart';
import 'main_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool hidePass = true;
  late List<User> user;

  @override
  void initState() {
    refreshNotes();
    super.initState();
  }

  Future refreshNotes() async {
    user = await UserDatabase.instance.readAllUsers();

    for (int i = 0; i < user.length; i++) {
      print(user[i].id);
      print(user[i].email);
      print(user[i].password);
    }
  }

  prefAuth({required String emailAuth}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString('emailAuth', emailAuth);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _form,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.language_outlined,
              size: 70,
              color: Colors.black54,
            ),
            const Text(
              "LOGIN",
              style: TextStyle(color: Colors.black54, fontSize: 30),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 30),
                      child: Text(
                        'Email',
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.black12),
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(26, 5, 26, 0),
                      child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: TextFormField(
                          controller: emailController,
                          validator: (val) {
                            bool emailValid = RegExp(
                                    r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$')
                                .hasMatch(emailController.text);

                            if (val!.isEmpty) {
                              return 'Input your email';
                            }
                            if (emailValid == false) {
                              return 'format is wrong';
                            }

                            return null;
                          },
                          decoration: const InputDecoration(
                              border: InputBorder.none, hintText: 'Your email'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 30),
                      child: Text(
                        'Password',
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.black12),
                      width: double.infinity,
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      margin: const EdgeInsets.fromLTRB(25, 5, 26, 0),
                      child: SizedBox(
                        width: double.infinity,
                        child: TextFormField(
                          controller: passwordController,
                          validator: (val) {
                            if (val!.isEmpty) {
                              return 'input your password';
                            }
                            return null;
                          },
                          obscureText: hidePass,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Password',
                            suffixIcon: IconButton(
                              icon: Icon(
                                hidePass
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Colors.black,
                                size: 20,
                              ),
                              onPressed: () {
                                setState(() {
                                  hidePass = !hidePass;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(18, 25, 18, 10),
                      child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(8),
                          child: ElevatedButton(
                              child: const Padding(
                                padding: EdgeInsets.all(14.0),
                                child: Text(
                                  'Login',
                                ),
                              ),
                              onPressed: () {
                                if (_form.currentState!.validate() == true) {
                                  var a = user.where((element) =>
                                      element.email == emailController.text &&
                                      element.password ==
                                          passwordController.text);

                                  if (a.isEmpty) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content:
                                            Text("wrong username/password"),
                                      ),
                                    );
                                  } else {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                const MainPage()),
                                        (Route<dynamic> route) =>
                                            route is MainPage);
                                    prefAuth(emailAuth: emailController.text);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                            'Wellcome ${emailController.text} !'),
                                      ),
                                    );
                                  }
                                }
                              },
                              style: ButtonStyle(
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          whiteColor),
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.black87),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ))))),
                    ),
                  ],
                ),
              ),
            ),
            TextButton(
              onPressed: () =>
                  Navigator.of(context).pushNamed(RegisterScreen.routerName),
              child: const Text(
                'Register Now!',
                style: TextStyle(color: Colors.black54),
              ),
            )
          ],
        ),
      ),
    );
  }
}
