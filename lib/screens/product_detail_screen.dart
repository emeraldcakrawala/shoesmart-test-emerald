import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:shoesmart_test/providers/cart.dart';
import 'package:shoesmart_test/screens/cart_screen.dart';
import 'package:shoesmart_test/shared/shared.dart';
import 'package:shoesmart_test/widgets/badge.dart';

import '../providers/product.dart';
import '../providers/products.dart';

class ProductDetailScreen extends StatelessWidget {
  static const routerName = '/product-detail';

  @override
  Widget build(BuildContext context) {
    final productId = ModalRoute.of(context)!.settings.arguments as String;
    final productsData = Provider.of<Products>(context);

    final selectedProduct = productsData.findById(productId);

    return Scaffold(
      bottomNavigationBar: Consumer<Cart>(
          builder: (ctx, cart, _) => Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 8),
                child: ElevatedButton(
                    onPressed: () {
                      cart.addItem(
                          productId: selectedProduct.id,
                          price: selectedProduct.price,
                          productName: selectedProduct.productName,
                          brandName: selectedProduct.brandName,
                          imageAsset: selectedProduct.imageAsset);
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                            '${selectedProduct.productName} has been added to cart.',
                            textAlign: TextAlign.center,
                          ),
                          duration: const Duration(seconds: 2),
                          action: SnackBarAction(
                            label: 'Undo',
                            textColor: Colors.white,
                            onPressed: () => cart.removeSingleItem(
                              selectedProduct.id,
                            ),
                          ),
                        ),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(
                            Icons.shopping_cart,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text('Add to Cart')
                        ],
                      ),
                    ),
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(whiteColor),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.black87),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        )))),
              )),
      appBar: AppBar(
        backgroundColor: Colors.black54,
        actions: [
          Consumer<Cart>(
            builder: (ctx, cart, constantChild) => Badge(
              color: Colors.red,
              child: constantChild!,
              value: cart.itemCount.toString(),
            ),
            child: IconButton(
              icon: const Icon(Icons.shopping_cart),
              onPressed: () =>
                  Navigator.of(context).pushNamed(CartScreen.routerName),
            ),
          ),
        ],
        title: Text(selectedProduct.productName),
      ),
      body: ChangeNotifierProvider.value(
        value: selectedProduct,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 300,
                width: double.infinity,
                child: Image.asset(
                  selectedProduct.imageAsset,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${selectedProduct.productName}',
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            '${selectedProduct.brandName}',
                            style: const TextStyle(
                                color: Colors.grey,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Rp ${selectedProduct.price}',
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            color: Colors.black,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                            'Description',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            selectedProduct.description,
                            softWrap: true,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Consumer<Product>(
                    builder: (ctx, product, _) => IconButton(
                      icon: Icon(
                        product.isFavorite
                            ? Icons.favorite
                            : Icons.favorite_outline,
                      ),
                      color: Colors.red,
                      onPressed: selectedProduct.toggleFavoriteStatus,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
