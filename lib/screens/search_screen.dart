import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoesmart_test/providers/product.dart';
import 'package:shoesmart_test/shared/shared.dart';
import 'package:shoesmart_test/widgets/product_item.dart';
import 'package:supercharged/supercharged.dart';
import '../providers/products.dart';

class SearchPage extends StatefulWidget {
  static const routerName = '/search-page';
  SearchPage({Key? key, this.brand}) : super(key: key);
  String? brand;

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController controller = TextEditingController();

  FocusNode inputNode = FocusNode();

  @override
  void initState() {
    if (widget.brand != '') {
      controller.text = widget.brand ?? '';
      Timer(Duration.zero, () => onSearchTextChanged(widget.brand ?? ''));
    }
    super.initState();
  }

  List<Product> searchResult = [];
  List<Product> productDetails = [];

  onSearchTextChanged(String text) async {
    searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    for (var productDetail in productDetails) {
      if (productDetail.productName
              .toLowerCase()
              .contains(text.toLowerCase()) ||
          productDetail.brandName.toLowerCase().contains(text.toLowerCase())) {
        searchResult.add(productDetail);
      }
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    setState(() {
      productDetails = productsData.items;
    });
    return Scaffold(
        body: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
          child: Container(
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: '7783EF'.toColor().withOpacity(0.1),
                spreadRadius: 0,
                blurRadius: 16,
                offset: const Offset(0, 4),
              ),
            ], borderRadius: BorderRadius.circular(20), color: whiteColor),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(40)),
              child: ListTile(
                title: TextFormField(
                  autofocus: true,
                  focusNode: inputNode,
                  controller: controller,
                  decoration: InputDecoration(
                    prefixIcon: IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: fontColor,
                        size: 18,
                      ),
                    ),
                    hintText: 'search',
                    border: InputBorder.none,
                  ),
                  onChanged: onSearchTextChanged,
                ),
                trailing: controller.text.isEmpty
                    ? null
                    : IconButton(
                        icon: const Icon(Icons.cancel),
                        onPressed: () {
                          controller.clear();
                          onSearchTextChanged('');
                        },
                      ),
              ),
            ),
          ),
        ),
        Expanded(
            child: searchResult.isNotEmpty || controller.text.isNotEmpty
                ? GridView.builder(
                    padding: const EdgeInsets.all(10.0),
                    itemCount: searchResult.length,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 10 / 13,
                      mainAxisSpacing: 16,
                      crossAxisSpacing: 20,
                    ),
                    itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
                      value: searchResult[i],
                      child: ProductItem(),
                    ),
                  )
                : const Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      'Search Product or Branch Name',
                      textAlign: TextAlign.center,
                    ),
                  )),
      ],
    ));
  }
}
