import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoesmart_test/providers/cart.dart';
import 'package:shoesmart_test/providers/products.dart';
import 'package:shoesmart_test/screens/cart_screen.dart';
import 'package:shoesmart_test/widgets/badge.dart';
import '../widgets/products_list.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  int sList = 4;
  bool isGrid = true;

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black54,
          title: const Text('Favorite'),
        ),
        body: Column(
          children: [
            Card(
              margin: const EdgeInsets.all(15),
              child: Container(
                padding: const EdgeInsets.all(15),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Total :',
                      style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.normal,
                          fontSize: 14),
                    ),
                    Text(
                      '${productsData.favItems.length} ${productsData.favItems.length == 1 ? 'Item' : 'Items'}',
                      style: const TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.normal,
                          fontSize: 14),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
                child: ProductList(
              sList,
              isGrid ? 2 : 1,
              99999999999,
              0,
            )),
            SizedBox(
              height: MediaQuery.of(context).size.height / 15,
            )
          ],
        ));
  }
}
