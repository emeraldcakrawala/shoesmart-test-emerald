import 'dart:async';

import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shoesmart_test/shared/shared.dart';
import 'package:shoesmart_test/widgets/alert.dart';
import 'package:supercharged/supercharged.dart';

import 'login_screen.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  void initState() {
    Timer(Duration.zero, () {
      getDataUser();
    });
    super.initState();
  }

  logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('emailAuth');

    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Logout successfully'),
      ),
    );

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => const LoginScreen()),
        (Route<dynamic> route) => false);
  }

  Widget accnLst() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () => Alert().showAlertDialog2Botton(
              context: context,
              title: 'Logout',
              subtitle: 'Are you sure to logout?',
              text2: 'Yes',
              textCancel: 'No',
              onPressed2: () => Timer(
                    Duration.zero,
                    () => logout(),
                  )),
          child: Container(
            padding: const EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: '7783EF'.toColor().withOpacity(0.1),
                  spreadRadius: 0,
                  blurRadius: 16,
                  offset: const Offset(0, 4),
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text('LogOut'),
                Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget pkgInfo() {
    return FutureBuilder(
        future: PackageInfo.fromPlatform(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return Text(
              'e-commerce v.${snapshot.data.version}',
              textAlign: TextAlign.center,
            );
          }
          return Container();
        });
  }

  String? email;

  getDataUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final emailP = prefs.getString('emailAuth');
    setState(() {
      email = emailP;
    });
  }

  Widget childMenu({
    required String title,
    required String content,
  }) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 8, 15, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
          ),
          Text(
            content,
          ),
        ],
      ),
    );
  }

  Widget expansion() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: '7783EF'.toColor().withOpacity(0.1),
              spreadRadius: 0,
              blurRadius: 16,
              offset: const Offset(0, 4),
            ),
          ],
        ),
        child: ExpansionTile(
          title: const Text(
            'Developer Information',
          ),
          children: [
            childMenu(title: 'Developer', content: 'Emerald Shan Cakrawala'),
            childMenu(title: 'Email', content: '@emeraldsc08@gmail.com'),
            childMenu(title: 'Phone', content: '+62 822 341 503 21'),
          ],
        ),
      ),
    );
  }

  Widget dataUser() {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        const Icon(
          Icons.person_pin,
          color: Colors.black54,
          size: 90,
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          email ?? '',
          style: const TextStyle(color: Colors.black54, fontSize: 16),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black54,
          title: const Text('Profile'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              dataUser(),
              const SizedBox(
                height: 20,
              ),
              accnLst(),
              expansion(),
              SizedBox(
                height: MediaQuery.of(context).size.height / 15,
              ),
              pkgInfo()
            ],
          ),
        ));
  }
}
