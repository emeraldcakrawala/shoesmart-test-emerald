import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoesmart_test/providers/products.dart';
import 'package:shoesmart_test/screens/search_screen.dart';
import 'package:shoesmart_test/shared/shared.dart';
import 'package:supercharged/supercharged.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import './cart_screen.dart';
import '../widgets/products_list.dart';
import '../widgets/badge.dart';
import '../widgets/main_drawer.dart';
import '../providers/cart.dart';

class ProductsOverviewScreen extends StatefulWidget {
  const ProductsOverviewScreen({Key? key}) : super(key: key);

  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  int sList = 0;
  bool isGrid = true;

  TextEditingController upController = TextEditingController();
  TextEditingController downController = TextEditingController();

  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  Widget searchButton() {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 1.5,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SearchPage(),
            ),
          ),
          // onTap: () => Navigator.of(context).pushNamed(SearchPage.routerName),
          child: Container(
            padding: const EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: '7783EF'.toColor().withOpacity(0.1),
                  spreadRadius: 0,
                  blurRadius: 16,
                  offset: const Offset(0, 4),
                ),
              ],
            ),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  color: fontColor,
                  size: 16,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text('Search',
                    style: TextStyle(fontSize: 12, color: fontColor)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    var ids = productsData.items;
    List brand = [];
    for (int i = 0; i < ids.length; i++) {
      brand.add(ids[i].brandName);
    }
    var distinctIds = brand.toSet().toList();

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black54,
          actions: [
            searchButton(),
            Consumer<Cart>(
              builder: (ctx, cart, constantChild) => Badge(
                color: Colors.red,
                child: constantChild!,
                value: cart.itemCount.toString(),
              ),
              child: IconButton(
                icon: const Icon(Icons.shopping_cart),
                onPressed: () =>
                    Navigator.of(context).pushNamed(CartScreen.routerName),
              ),
            ),
          ],
        ),
        drawer: const MainDrawer(),
        body: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                    onPressed: () {
                      showBarModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        barrierColor: Colors.black.withOpacity(0.5),
                        backgroundColor: Colors.white,
                        builder: (builder) => Container(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          color: whiteColor,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Filter by Brand',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              const SizedBox(height: 10),
                              Flexible(
                                child: Wrap(
                                  children: [
                                    for (int i = 0; i < distinctIds.length; i++)
                                      Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: ElevatedButton(
                                            style: ButtonStyle(
                                                foregroundColor:
                                                    MaterialStateProperty.all<
                                                        Color>(whiteColor),
                                                backgroundColor:
                                                    MaterialStateProperty.all<
                                                        Color>(Colors.black54),
                                                shape: MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                    RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ))),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      SearchPage(
                                                    brand: distinctIds[i],
                                                  ),
                                                ),
                                              );
                                            },
                                            child: Text(distinctIds[i])),
                                      )
                                  ],
                                ),
                              ),
                              const SizedBox(height: 20),
                              const Text(
                                'Filter by Range',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              const SizedBox(height: 10),
                              Form(
                                key: _form,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Padding(
                                      padding: EdgeInsets.only(
                                          left: 30, bottom: 5, top: 10),
                                      child: Text(
                                        'min range',
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          color: Colors.grey[200]),
                                      width: double.infinity,
                                      margin: const EdgeInsets.fromLTRB(
                                          26, 5, 26, 0),
                                      child: Container(
                                        width: double.infinity,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15),
                                        child: TextFormField(
                                          keyboardType: TextInputType.number,
                                          controller: downController,
                                          validator: (val) {
                                            if (val!.isEmpty ||
                                                upController.text.toDouble()! <
                                                    downController.text
                                                        .toDouble()!) {
                                              return 'enter your min range correctly';
                                            }
                                            return null;
                                          },
                                          decoration: const InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'your min range'),
                                        ),
                                      ),
                                    ),
                                    const Padding(
                                      padding:
                                          EdgeInsets.only(left: 30, bottom: 5),
                                      child: Text(
                                        'max range',
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          color: Colors.grey[200]),
                                      width: double.infinity,
                                      margin: const EdgeInsets.fromLTRB(
                                          26, 5, 26, 0),
                                      child: Container(
                                        width: double.infinity,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15),
                                        child: TextFormField(
                                          keyboardType: TextInputType.number,
                                          controller: upController,
                                          validator: (val) {
                                            if (val!.isEmpty ||
                                                upController.text.toDouble()! <
                                                    downController.text
                                                        .toDouble()!) {
                                              return 'enter your max range correctly';
                                            }
                                            return null;
                                          },
                                          decoration: const InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'your max range'),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          18, 25, 18, 10),
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: ElevatedButton(
                                            style: ButtonStyle(
                                                foregroundColor:
                                                    MaterialStateProperty.all<
                                                        Color>(whiteColor),
                                                backgroundColor:
                                                    MaterialStateProperty.all<
                                                        Color>(Colors.black54),
                                                shape: MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                    RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ))),
                                            onPressed: () {
                                              if (_form.currentState!
                                                      .validate() ==
                                                  true) {
                                                setState(() {
                                                  sList = 1;
                                                });
                                                Navigator.pop(context);
                                              }
                                            },
                                            child: const Text("Enter")),
                                      ),
                                    ),
                                    if (sList == 1)
                                      Center(
                                        child: GestureDetector(
                                            onTap: () {
                                              upController.clear();
                                              downController.clear();
                                              setState(() {
                                                sList = 0;
                                              });
                                              Navigator.pop(context);
                                            },
                                            child: const Text(
                                              'Reset Filter',
                                              textAlign: TextAlign.center,
                                            )),
                                      )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    child: Row(
                      children: const [
                        Text(
                          'Filter',
                          style: TextStyle(color: Colors.black),
                        ),
                        Icon(
                          Icons.filter_alt_rounded,
                          size: 20,
                          color: Colors.black,
                        ),
                      ],
                    )),
                TextButton(
                    onPressed: () {
                      showBarModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        barrierColor: Colors.black.withOpacity(0.5),
                        backgroundColor: Colors.white,
                        builder: (builder) => Container(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          color: whiteColor,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextButton(
                                onPressed: () {
                                  setState(() {
                                    sList = 2;
                                  });
                                  Navigator.pop(context);
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text(
                                      'Sort by Cheapest',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                    Icon(
                                        sList == 2
                                            ? Icons.circle
                                            : Icons.circle_outlined,
                                        size: 10,
                                        color: Colors.black)
                                  ],
                                ),
                              ),
                              const SizedBox(height: 10),
                              TextButton(
                                onPressed: () {
                                  setState(() {
                                    sList = 3;
                                  });
                                  Navigator.pop(context);
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text(
                                      'Sort by most expensive',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 16),
                                    ),
                                    Icon(
                                        sList == 3
                                            ? Icons.circle
                                            : Icons.circle_outlined,
                                        size: 10,
                                        color: Colors.black)
                                  ],
                                ),
                              ),
                              const SizedBox(height: 20),
                              if (sList == 2 || sList == 3)
                                TextButton(
                                    onPressed: () {
                                      upController.clear();
                                      downController.clear();
                                      setState(() {
                                        sList = 0;
                                      });
                                      Navigator.pop(context);
                                    },
                                    child: const Text(
                                      'Reset Filter',
                                      style: TextStyle(color: Colors.black54),
                                    ))
                            ],
                          ),
                        ),
                      );
                    },
                    child: Row(
                      children: const [
                        Text(
                          'Sort',
                          style: TextStyle(color: Colors.black),
                        ),
                        Icon(
                          Icons.sort,
                          size: 20,
                          color: Colors.black,
                        ),
                      ],
                    )),
                IconButton(
                    onPressed: () {
                      setState(() {
                        isGrid = !isGrid;
                      });
                    },
                    icon: Icon(isGrid
                        ? Icons.grid_view_outlined
                        : Icons.list_rounded)),
              ],
            ),
            Expanded(
                child: ProductList(
              sList,
              isGrid ? 2 : 1,
              upController.text.toDouble() ?? 99999999999,
              downController.text.toDouble() ?? 0,
            )),
            SizedBox(
              height: MediaQuery.of(context).size.height / 15,
            )
          ],
        ));
  }
}
