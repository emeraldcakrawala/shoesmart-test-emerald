import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:provider/provider.dart';
import 'package:shoesmart_test/shared/shared.dart';

import '../providers/cart.dart' show Cart;
import '../providers/orders.dart';
import '../widgets/cart_item.dart';
import 'product_detail_screen.dart';

class CartScreen extends StatelessWidget {
  static const routerName = '/cart';

  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Chart'),
        backgroundColor: Colors.black54,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total : ${cart.itemCount} ${cart.itemCount == 1 ? 'Item' : 'Items'}',
                  textAlign: TextAlign.end,
                  style: const TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.normal,
                      fontSize: 14),
                ),
                const Text(
                  'swipe left product to delete',
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.normal,
                      fontSize: 12),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Expanded(
            child: cart.items.values.toList().isEmpty
                ? const Center(child: Text('no data'))
                : GroupedListView<dynamic, String>(
                    elements: cart.items.values.toList(),
                    groupBy: (brand) => brand.brandName,
                    groupComparator: (value1, value2) =>
                        value2.compareTo(value1),
                    itemComparator: (item1, item2) =>
                        item1.productName.compareTo(item2.productName),

                    // useStickyGroupSeparators: true,
                    indexedItemBuilder: (ctx, e, i) {
                      return CartItem(
                        id: e.id,
                        productName: e.productName,
                        brandName: e.brandName,
                        price: e.price,
                        quantity: e.quantity,
                        imageAsset: e.imageAsset,
                        productId: cart.items.keys.toList()[i],
                      );
                    },
                    groupSeparatorBuilder: (String value) => Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 0, 10),
                      child: Text(
                        value,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
          ),
          Card(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Grand Total : ',
                    style: TextStyle(fontSize: 16, color: Colors.black54),
                  ),
                  const Spacer(),
                  Text(
                    'Rp ${cart.totalAmount.toStringAsFixed(2)}',
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              onPressed: () {
                if (cart.items.values.toList().isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('Select items to buy!'),
                  ));
                } else {
                  Provider.of<Orders>(context, listen: false).addOrder(
                    cart.items.values.toList(),
                    cart.totalAmount,
                  );
                  cart.clear();
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('Checkout successful'),
                  ));
                }
              },
              child: const Text('Checkout'),
              style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all<Color>(whiteColor),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.black87),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ))),
            ),
          )
        ],
      ),
    );
  }
}
