import 'package:flutter/material.dart';
import 'package:supercharged/supercharged.dart';

Color mainColor = "73A3FE".toColor();
Color yellowColor = "6e5221".toColor();
Color greyColor = "8D92A3".toColor();
Color darkBlueColor = "1C316B".toColor();
Color backroundColor = "F3F8FE".toColor();
Color whiteColor = "FFFFFF".toColor();
Color greenColor = "00D3A1".toColor();
Color redColor = Colors.red;
Color blueColor = "0000FF".toColor();
Color orangeColor = "FFAF37".toColor();
Color softGreyColor = "E2E2E2".toColor();
Color fontColor = "494949".toColor().withOpacity(0.8);
Color buttonColor = "73A3FE".toColor();

const double defaultMargin = 24;
