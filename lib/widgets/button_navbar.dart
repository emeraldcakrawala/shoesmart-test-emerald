import 'package:flutter/material.dart';
import 'package:shoesmart_test/shared/shared.dart';
import 'package:supercharged/supercharged.dart';

class ButtonNavBar extends StatelessWidget {
  final int selectedIndex;
  final Function(int index) onTap;

  ButtonNavBar({required this.onTap, this.selectedIndex = 0});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: whiteColor,
          boxShadow: [
            BoxShadow(
              color: '7783EF'.toColor().withOpacity(0.1),
              spreadRadius: 0,
              blurRadius: 16,
              offset: const Offset(0, 4),
            ),
          ],
        ),
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
                onPressed: () => onTap(0),
                icon: Icon(
                  selectedIndex == 0
                      ? Icons.holiday_village_rounded
                      : Icons.holiday_village_outlined,
                  color: Colors.black87,
                )),
            IconButton(
                onPressed: () => onTap(1),
                icon: Icon(
                  selectedIndex == 1
                      ? Icons.favorite
                      : Icons.favorite_border_outlined,
                  color: Colors.black87,
                )),
            IconButton(
                onPressed: () => onTap(3),
                icon: Icon(
                  selectedIndex == 2
                      ? Icons.account_circle
                      : Icons.account_circle_outlined,
                  color: Colors.black87,
                )),
          ],
        ),
      ),
    );
  }
}
