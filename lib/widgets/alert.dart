import 'package:flutter/material.dart';

class Alert {
  showAlertDialog2Botton({
    required BuildContext context,
    required void Function() onPressed2,
    required String textCancel,
    required String text2,
    required String title,
    required String subtitle,
  }) {
    // set up the buttons
    Widget cancelButton = GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(textCancel),
      ),
      onTap: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(text2),
      ),
      onTap: onPressed2,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Center(
          child: Text(
        title,
      )),
      content: Text(
        subtitle,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
