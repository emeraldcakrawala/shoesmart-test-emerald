import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './product_item.dart';
import '../providers/products.dart';

class ProductList extends StatelessWidget {
  //null & 0 = SHOW ALL
  //1 = SHOW RANGE
  //2 = SORT cheapest
  //3 = SORT most expensive
  //4 = SHOW FAVORITE
  final int sList;
  final int isGrid;
  final double up;
  final double down;

  const ProductList(this.sList, @required this.isGrid, this.up, this.down);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);

    final products = sList == 1
        ? productsData.getRange(rangeDown: down, rangeUp: up)
        : sList == 2
            ? productsData.sortCheapest
            : sList == 3
                ? productsData.sortCheapest.reversed.toList()
                : sList == 4
                    ? productsData.favItems
                    : productsData.items;

    return products.isEmpty
        ? const Center(child: Text('No data'))
        : GridView.builder(
            padding: const EdgeInsets.all(10.0),
            itemCount: products.length,
            itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
              value: products[i],
              child: ProductItem(),
            ),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: isGrid,
              childAspectRatio: 10 / 13,
              mainAxisSpacing: 16,
              crossAxisSpacing: 20,
            ),
          );
  }
}
