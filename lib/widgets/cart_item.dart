import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoesmart_test/screens/product_detail_after_screen.dart';
import 'package:shoesmart_test/screens/product_detail_screen.dart';

import '../providers/cart.dart';

class CartItem extends StatelessWidget {
  final String id;
  final double price;
  final int quantity;
  final String productName;
  final String brandName;
  final String productId;
  final String imageAsset;

  CartItem({
    required this.id,
    required this.price,
    required this.quantity,
    required this.productName,
    required this.brandName,
    required this.productId,
    required this.imageAsset,
  });

  bool _isChecked = true;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(id),
      background: Container(
        color: Theme.of(context).errorColor,
        child: const Icon(
          Icons.delete,
          color: Colors.white,
          size: 32,
        ),
        alignment: Alignment.centerRight,
        padding: const EdgeInsets.only(right: 20),
        margin: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: const Text('Are you sure?'),
            content:
                const Text('Do you want to remove the item from the cart?'),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(ctx).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () => Navigator.of(ctx).pop(true),
                child: const Text('Yes'),
              ),
            ],
          ),
        );
      },
      onDismissed: (direction) {
        final cart = Provider.of<Cart>(context, listen: false);
        cart.removeItem(productId);
      },
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => ProductDetailAfterScreen(
                      id: id,
                      brandName: brandName,
                      price: price,
                      productName: productName,
                      quantity: quantity,
                      imageAsset: imageAsset,
                    )),
          );
        },
        child: Card(
          margin: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 4,
          ),
          child: Row(
            children: [
              CheckBoxItem(),
              Expanded(
                child: ListTile(
                  contentPadding: const EdgeInsets.all(10),
                  title: Text(
                    productName,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.normal),
                  ),
                  subtitle: Text('$price x $quantity'),
                  trailing: Text('${price * quantity}'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CheckBoxItem extends StatefulWidget {
  CheckBoxItem({
    Key? key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<CheckBoxItem> {
  bool _myBoolean = true;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Checkbox(
        value: _myBoolean,
        activeColor: Colors.black54,
        onChanged: (value) {
          // setState(() {
          //   _myBoolean = value!;
          // });
        },
      ),
    );
  }
}
