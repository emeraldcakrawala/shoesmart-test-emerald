import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/product.dart';
import '../providers/products.dart';

class ProductForm extends StatefulWidget {
  final Product selectedProduct;

  ProductForm({
    required this.selectedProduct,
  });

  @override
  _ProductFormState createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {
  final _formKey = GlobalKey<FormState>();

  final _productNameController = TextEditingController();
  final _brandNameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _priceController = TextEditingController();
  final _imageAssetController = TextEditingController();

  @override
  void initState() {
    if (widget.selectedProduct != null) {
      _productNameController.text = widget.selectedProduct.productName;
      _brandNameController.text = widget.selectedProduct.brandName;
      _descriptionController.text = widget.selectedProduct.description;
      _priceController.text = widget.selectedProduct.price.toString();
      _imageAssetController.text = widget.selectedProduct.imageAsset;
    }
    return super.initState();
  }

  @override
  void dispose() {
    _productNameController.dispose();
    _brandNameController.dispose();
    _descriptionController.dispose();
    _priceController.dispose();
    _imageAssetController.dispose();
    super.dispose();
  }

  void _handleSubmit() {
    final payload = Product(
      id: (widget.selectedProduct != null &&
              widget.selectedProduct.id.isNotEmpty)
          ? widget.selectedProduct.id
          : DateTime.now().toString(),
      productName: _productNameController.text,
      brandName: _brandNameController.text,
      description: _descriptionController.text,
      price: double.parse(_priceController.text),
      imageAsset: _imageAssetController.text,
    );

    if (widget.selectedProduct != null) {
      Provider.of<Products>(context, listen: false)
          .editProduct(widget.selectedProduct.id, payload);
    } else {
      Provider.of<Products>(context, listen: false).addProduct(payload);
    }

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          children: <Widget>[
            TextFormField(
              controller: _productNameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(12),
                ),
                labelText: 'Title',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Title cannot be empty';
                }
                return null;
              },
            ),
            SizedBox(height: 10),
            TextFormField(
              controller: _brandNameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(12),
                ),
                labelText: 'Brand',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Brand cannot be empty';
                }
                return null;
              },
            ),
            SizedBox(height: 10),
            TextFormField(
              controller: _descriptionController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(12),
                ),
                labelText: 'Description',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Description cannot be empty';
                }
                return null;
              },
            ),
            SizedBox(height: 10),
            TextFormField(
              controller: _priceController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(12),
                ),
                labelText: 'Price',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Price cannot be empty';
                }
                return null;
              },
            ),
            SizedBox(height: 10),
            TextFormField(
              controller: _imageAssetController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(12),
                ),
                labelText: 'Image URL',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Image URL cannot be empty';
                }
                return null;
              },
            ),
            SizedBox(height: 10),
            Container(
              alignment: Alignment.centerRight,
              child: ElevatedButton(
                onPressed: () {
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }

                  _handleSubmit();
                },
                child: Text('Submit'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
