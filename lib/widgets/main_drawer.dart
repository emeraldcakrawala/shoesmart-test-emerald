import 'package:flutter/material.dart';

import '../screens/orders_screen.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppBar(
              title: const Text('e-commerce'),
              backgroundColor: Colors.black54,
              automaticallyImplyLeading: false,
            ),
            const Divider(),
            ListTile(
              onTap: () => Navigator.of(context).pushReplacementNamed('/'),
              leading: const Icon(
                Icons.shop,
                color: Colors.black,
              ),
              title: const Text(
                'Shop',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const Divider(),
            ListTile(
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(OrdersScreen.routerName),
              leading: const Icon(
                Icons.payment,
                color: Colors.black,
              ),
              title: const Text(
                'Orders',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const Divider(),
          ],
        ),
      ),
    );
  }
}
