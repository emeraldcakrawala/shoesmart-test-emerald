import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoesmart_test/screens/product_detail_after_screen.dart';
import 'package:shoesmart_test/screens/register_screen.dart';
import 'package:shoesmart_test/screens/search_screen.dart';
import 'package:shoesmart_test/screens/splash_screen.dart';
import './screens/cart_screen.dart';
import './screens/orders_screen.dart';
import './screens/product_detail_screen.dart';
import './providers/products.dart';
import './providers/cart.dart';
import './providers/orders.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Products(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Cart(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Orders(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'e-commerce',
        theme: ThemeData(
            fontFamily: 'Poppins',
            colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.indigo)),
        home: const SplashScreen(),
        routes: {
          RegisterScreen.routerName: (ctx) => const RegisterScreen(),
          SearchPage.routerName: (ctx) => SearchPage(),
          ProductDetailScreen.routerName: (ctx) => ProductDetailScreen(),
          ProductDetailAfterScreen.routerName: (ctx) =>
              ProductDetailAfterScreen(),
          CartScreen.routerName: (ctx) => const CartScreen(),
          OrdersScreen.routerName: (ctx) => const OrdersScreen(),
        },
      ),
    );
  }
}
